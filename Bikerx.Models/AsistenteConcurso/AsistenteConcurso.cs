﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.AsistenteConcurso
{
    using Bikerx.Models.Ciclista;
    using Bikerx.Models.Concurso;

    public class AsistenteConcurso : EntityBase<int>
    {
        //*Prop*

        public float LatitudCiclista { get; set; }
        public float LongitudCiclista { get; set; }

        //*LLaves*

        public int ConcursoId { get; set; }
        public Concurso Concurso { get; set; }

        public int CiclistaId { get; set; }
        public Ciclista Ciclista { get; set; }

        
    }
}
