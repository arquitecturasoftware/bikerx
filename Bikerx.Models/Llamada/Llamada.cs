﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.Llamada
{
    using Bikerx.Models.AsistenteRuta;

    public class Llamada : EntityBase<int>
    {
        //Propiedades
        public float LatitudCiclista { get; set; }
        public float LongitudCiclista { get; set; }

        //ForeingKeys
        public int AsistenteRutaId { get; set; }
        public AsistenteRuta AsistenteRuta { get; set; }


    }
}
