﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.Fotografia
{
    using Bikerx.Models.Ruta;
    using Bikerx.Models.CiclistaPro;

    public class Fotografia : EntityBase<int>
    {
        //*Prop*
        public string UrlImagen { get; set; }
        public string Descripcion { get; set; }

        //*Llaves*

        public int RutaId { get; set; }
        public Ruta Ruta { get; set; }

        public int CiclistaProId { get; set; }
        public CiclistaPro CiclistaPro { get; set; }

    }
}
