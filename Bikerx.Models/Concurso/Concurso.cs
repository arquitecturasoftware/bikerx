﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.Concurso
{
    using Bikerx.Models.Ruta;
    using Bikerx.Models.Comerciante;
    using Bikerx.Models.AsistenteConcurso;

    public class Concurso : EntityBase<int>
    {
        //Prop
        public string Descripcion { get; set; }
        public string UrlImagen { get; set; }
        public string Preparacion { get; set; }
        public bool HaTerminado { get; set; }
        public float ValorInscripcion { get; set; }

        public int ComercianteId { get; set; }
        public Comerciante Comerciante { get; set; }

        //Entidades

        public List<Ruta> Rutas { get; set; }
        public List<AsistenteConcurso> AsistentesConcurso { get; set; }
    }
}
