﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.Puntos_interes
{
    using Bikerx.Models.Ciclista;
    public class PuntodeInteres : EntityBase<int>
    {
        //Propiedades
        public float LatitudCiclista { get; set; }
        public float LongitudCiclista { get; set; }
        public string Descripcion { get; set; }



        // ForeingKeys
        public int CiclistaId { get; set; }
        public Ciclista Ciclista { get; set; }

        //Entidades Dependientes
    }
}
