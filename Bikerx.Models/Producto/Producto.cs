﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Bikerx.Models.Producto
{
    using Bikerx.Models.Comerciante;
    public class Producto : EntityBase<int>
    {
        //Propiedades
        public string Nombre { get; set; }
        public string UrlImagen { get; set; }
        public float Precio { get; set; }
        public int Puntos { get; set; }
        public bool Redimible { get; set; }
        public DateTime FechaDeVencimiento { get; set; }

        public int ComercianteId { get; set; }
        public Comerciante Comerciante { get; set; }
    }
}
