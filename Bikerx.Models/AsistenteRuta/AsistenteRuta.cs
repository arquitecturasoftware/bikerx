﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.AsistenteRuta
{
    using Bikerx.Models.Ruta;
    using Bikerx.Models.Ciclista;
    public class AsistenteRuta : EntityBase<int>
    {
        public int RutaId { get; set; }
        public Ruta Ruta { get; set; }

        public int CiclistaId { get; set; }
        public Ciclista Ciclista { get; set; }
        public string KmRecorridos { get; set; }
    }
}
