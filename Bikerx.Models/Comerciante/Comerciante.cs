﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.Comerciante
{
    using Bikerx.Models.Ciclista;
    public class Comerciante : EntityBase<int>
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Direccion { get; set; }
        public string NombreComercio { get; set; }
        public string TelefonoComercio { get; set; }

        public int CiclistaId { get; set; }
        public Ciclista Ciclista { get; set; }

        public float LatitudTienda { get; set; }
        public float LongitudTienda { get; set; }
    }
}
