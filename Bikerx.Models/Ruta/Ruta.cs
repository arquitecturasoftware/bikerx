﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Bikerx.Models.Ruta
{
    using Bikerx.Models.Fotografia;
    using Bikerx.Models.Concurso;
    using Bikerx.Models.AsistenteRuta;
    using Bikerx.Models.PrivilegioRuta;
    public class Ruta : EntityBase<int>
    {
        //*Prop*
        public string PosicionInicio { get; set; }
        public string PosicionFinal { get; set; }
        public DateTime FechaSalida { get; set; }
        public float KmRecorridosAprox { get; set; }

        //*LLaves*

        public int? ConcursoId { get; set; }
        public Concurso Concurso { get; set; }

        //*Entidades*

        public List<AsistenteRuta> AsistentesRuta { get; set; }
        public List<PrivilegioRuta> PrivilegiosRuta { get; set; }
        public List<Fotografia> Fotografias { get; set; }



    }
}
