﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.CiclistaPro
{
    using Bikerx.Models.Ciclista;
    public class CiclistaPro : EntityBase<int>
    {
        public int CiclistaId { get; set; }
        public Ciclista Ciclista { get; set; }


        public int MultiplicadorPuntos { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public bool Activo { get; set; }
    }
}
