﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.Models.PrivilegioRuta
{
    using Bikerx.Models.Ruta;
    using Bikerx.Models.CiclistaPro;
    using Bikerx.Models.AsistenteRuta;
    public class PrivilegioRuta : EntityBase<int>
    {
        public bool EsPrivado { get; set; }
        public string Calificacion { get; set; }

        public int RutaId { get; set; }
        public Ruta Ruta { get; set; }

        public int CiclistaProId { get; set; }
        public CiclistaPro CiclistaPro { get; set; }

        public int AsistenteRutaId { get; set; }
        public AsistenteRuta AsistenteRuta { get; set; }

        public string Comentarios { get; set; }
    }
}
