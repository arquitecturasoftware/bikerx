﻿using Bikerx.Models.AsistenteConcurso;
using Bikerx.Models.AsistenteRuta;
using Bikerx.Models.Ciclista;
using Bikerx.Models.CiclistaPro;
using Bikerx.Models.Comerciante;
using Bikerx.Models.Concurso;
using Bikerx.Models.Fotografia;
using Bikerx.Models.Llamada;
using Bikerx.Models.PrivilegioRuta;
using Bikerx.Models.Producto;
using Bikerx.Models.Puntos_interes;
using Bikerx.Models.Ruta;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bikerx.DataAccess
{
    public class BikerxDbContext : DbContext
    {
        public DbSet<AsistenteConcurso> AsistenteConcursos { get; set; }
        public DbSet<AsistenteRuta> AsistenteRutas { get; set; }
        public DbSet<Ciclista> Ciclistas { get; set; }
        public DbSet<CiclistaPro> CiclistaPros { get; set; }
        public DbSet<Comerciante> Comerciantes { get; set; }
        public DbSet<Concurso> Concursos { get; set; }
        public DbSet<Fotografia> Fotografias { get; set; }
        public DbSet<Llamada> Llamadas { get; set; }
        public DbSet<PrivilegioRuta> PrivilegioRutas { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<PuntodeInteres> PuntosdeInteres { get; set; }
        public DbSet<Ruta> Rutas { get; set; }

        public BikerxDbContext(DbContextOptions<BikerxDbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.RemovePluralizingTableNameConvention();
            Seed.SeedData(modelBuilder);
        }

    }
}
