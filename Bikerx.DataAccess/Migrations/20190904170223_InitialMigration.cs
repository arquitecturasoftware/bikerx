﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bikerx.DataAccess.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ciclista",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Nombres = table.Column<string>(nullable: true),
                    Apellidos = table.Column<string>(nullable: true),
                    NumeroContacto = table.Column<long>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Puntos = table.Column<int>(nullable: false),
                    AmigoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ciclista", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ciclista_Ciclista_AmigoId",
                        column: x => x.AmigoId,
                        principalTable: "Ciclista",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "CiclistaPro",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    CiclistaId = table.Column<int>(nullable: false),
                    MultiplicadorPuntos = table.Column<int>(nullable: false),
                    FechaInicio = table.Column<DateTime>(nullable: false),
                    FechaFinal = table.Column<DateTime>(nullable: false),
                    Activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CiclistaPro", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CiclistaPro_Ciclista_CiclistaId",
                        column: x => x.CiclistaId,
                        principalTable: "Ciclista",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comerciante",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Nombres = table.Column<string>(nullable: true),
                    Apellidos = table.Column<string>(nullable: true),
                    Direccion = table.Column<string>(nullable: true),
                    NombreComercio = table.Column<string>(nullable: true),
                    TelefonoComercio = table.Column<string>(nullable: true),
                    CiclistaId = table.Column<int>(nullable: false),
                    LatitudTienda = table.Column<float>(nullable: false),
                    LongitudTienda = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comerciante", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comerciante_Ciclista_CiclistaId",
                        column: x => x.CiclistaId,
                        principalTable: "Ciclista",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PuntodeInteres",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    LatitudCiclista = table.Column<float>(nullable: false),
                    LongitudCiclista = table.Column<float>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true),
                    CiclistaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PuntodeInteres", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PuntodeInteres_Ciclista_CiclistaId",
                        column: x => x.CiclistaId,
                        principalTable: "Ciclista",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Concurso",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true),
                    UrlImagen = table.Column<string>(nullable: true),
                    Preparacion = table.Column<string>(nullable: true),
                    HaTerminado = table.Column<bool>(nullable: false),
                    ValorInscripcion = table.Column<float>(nullable: false),
                    ComercianteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Concurso", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Concurso_Comerciante_ComercianteId",
                        column: x => x.ComercianteId,
                        principalTable: "Comerciante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Producto",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    UrlImagen = table.Column<string>(nullable: true),
                    Precio = table.Column<float>(nullable: false),
                    Puntos = table.Column<int>(nullable: false),
                    Redimible = table.Column<bool>(nullable: false),
                    FechaDeVencimiento = table.Column<DateTime>(nullable: false),
                    ComercianteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Producto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Producto_Comerciante_ComercianteId",
                        column: x => x.ComercianteId,
                        principalTable: "Comerciante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AsistenteConcurso",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    LatitudCiclista = table.Column<float>(nullable: false),
                    LongitudCiclista = table.Column<float>(nullable: false),
                    ConcursoId = table.Column<int>(nullable: false),
                    CiclistaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AsistenteConcurso", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AsistenteConcurso_Ciclista_CiclistaId",
                        column: x => x.CiclistaId,
                        principalTable: "Ciclista",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AsistenteConcurso_Concurso_ConcursoId",
                        column: x => x.ConcursoId,
                        principalTable: "Concurso",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Ruta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    PosicionInicio = table.Column<string>(nullable: true),
                    PosicionFinal = table.Column<string>(nullable: true),
                    FechaSalida = table.Column<DateTime>(nullable: false),
                    KmRecorridosAprox = table.Column<float>(nullable: false),
                    ConcursoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ruta", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ruta_Concurso_ConcursoId",
                        column: x => x.ConcursoId,
                        principalTable: "Concurso",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AsistenteRuta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    RutaId = table.Column<int>(nullable: false),
                    CiclistaId = table.Column<int>(nullable: false),
                    KmRecorridos = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AsistenteRuta", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AsistenteRuta_Ciclista_CiclistaId",
                        column: x => x.CiclistaId,
                        principalTable: "Ciclista",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AsistenteRuta_Ruta_RutaId",
                        column: x => x.RutaId,
                        principalTable: "Ruta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Fotografia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UrlImagen = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    RutaId = table.Column<int>(nullable: false),
                    CiclistaProId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fotografia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fotografia_CiclistaPro_CiclistaProId",
                        column: x => x.CiclistaProId,
                        principalTable: "CiclistaPro",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Fotografia_Ruta_RutaId",
                        column: x => x.RutaId,
                        principalTable: "Ruta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Llamada",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    LatitudCiclista = table.Column<float>(nullable: false),
                    LongitudCiclista = table.Column<float>(nullable: false),
                    AsistenteRutaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Llamada", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Llamada_AsistenteRuta_AsistenteRutaId",
                        column: x => x.AsistenteRutaId,
                        principalTable: "AsistenteRuta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrivilegioRuta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    EsPrivado = table.Column<bool>(nullable: false),
                    Calificacion = table.Column<string>(nullable: true),
                    RutaId = table.Column<int>(nullable: false),
                    CiclistaProId = table.Column<int>(nullable: false),
                    AsistenteRutaId = table.Column<int>(nullable: false),
                    Comentarios = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivilegioRuta", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrivilegioRuta_AsistenteRuta_AsistenteRutaId",
                        column: x => x.AsistenteRutaId,
                        principalTable: "AsistenteRuta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrivilegioRuta_CiclistaPro_CiclistaProId",
                        column: x => x.CiclistaProId,
                        principalTable: "CiclistaPro",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PrivilegioRuta_Ruta_RutaId",
                        column: x => x.RutaId,
                        principalTable: "Ruta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AsistenteConcurso_CiclistaId",
                table: "AsistenteConcurso",
                column: "CiclistaId");

            migrationBuilder.CreateIndex(
                name: "IX_AsistenteConcurso_ConcursoId",
                table: "AsistenteConcurso",
                column: "ConcursoId");

            migrationBuilder.CreateIndex(
                name: "IX_AsistenteRuta_CiclistaId",
                table: "AsistenteRuta",
                column: "CiclistaId");

            migrationBuilder.CreateIndex(
                name: "IX_AsistenteRuta_RutaId",
                table: "AsistenteRuta",
                column: "RutaId");

            migrationBuilder.CreateIndex(
                name: "IX_Ciclista_AmigoId",
                table: "Ciclista",
                column: "AmigoId");

            migrationBuilder.CreateIndex(
                name: "IX_CiclistaPro_CiclistaId",
                table: "CiclistaPro",
                column: "CiclistaId");

            migrationBuilder.CreateIndex(
                name: "IX_Comerciante_CiclistaId",
                table: "Comerciante",
                column: "CiclistaId");

            migrationBuilder.CreateIndex(
                name: "IX_Concurso_ComercianteId",
                table: "Concurso",
                column: "ComercianteId");

            migrationBuilder.CreateIndex(
                name: "IX_Fotografia_CiclistaProId",
                table: "Fotografia",
                column: "CiclistaProId");

            migrationBuilder.CreateIndex(
                name: "IX_Fotografia_RutaId",
                table: "Fotografia",
                column: "RutaId");

            migrationBuilder.CreateIndex(
                name: "IX_Llamada_AsistenteRutaId",
                table: "Llamada",
                column: "AsistenteRutaId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivilegioRuta_AsistenteRutaId",
                table: "PrivilegioRuta",
                column: "AsistenteRutaId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivilegioRuta_CiclistaProId",
                table: "PrivilegioRuta",
                column: "CiclistaProId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivilegioRuta_RutaId",
                table: "PrivilegioRuta",
                column: "RutaId");

            migrationBuilder.CreateIndex(
                name: "IX_Producto_ComercianteId",
                table: "Producto",
                column: "ComercianteId");

            migrationBuilder.CreateIndex(
                name: "IX_PuntodeInteres_CiclistaId",
                table: "PuntodeInteres",
                column: "CiclistaId");

            migrationBuilder.CreateIndex(
                name: "IX_Ruta_ConcursoId",
                table: "Ruta",
                column: "ConcursoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AsistenteConcurso");

            migrationBuilder.DropTable(
                name: "Fotografia");

            migrationBuilder.DropTable(
                name: "Llamada");

            migrationBuilder.DropTable(
                name: "PrivilegioRuta");

            migrationBuilder.DropTable(
                name: "Producto");

            migrationBuilder.DropTable(
                name: "PuntodeInteres");

            migrationBuilder.DropTable(
                name: "AsistenteRuta");

            migrationBuilder.DropTable(
                name: "CiclistaPro");

            migrationBuilder.DropTable(
                name: "Ruta");

            migrationBuilder.DropTable(
                name: "Concurso");

            migrationBuilder.DropTable(
                name: "Comerciante");

            migrationBuilder.DropTable(
                name: "Ciclista");
        }
    }
}
