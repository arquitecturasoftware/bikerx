Vue.component('pie_de_pagina',{
    template: `<footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>Todos los derechos reservados &copy; BikerX 2019</span>
      </div>
    </div>
  </footer>`,
});