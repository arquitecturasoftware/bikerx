Vue.component('nuevo_concurso', {
    data() {
        return {
            mapa: 'https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d63558.465941870825!2d-72.357522!3d5.355141000000001!3m2!1i1024!2i768!4f13.1!4m13!3e2!4m5!1s0x8e6b0c4ebccd7c3d%3A0xdc616326308242e5!2sLa%20Cascada%20Reposteria%2C%20Cl.%2011%20%2324%2068%2C%20Yopal%2C%20Casanare!3m2!1d5.3484938!2d-72.3949765!4m5!1s0x8e6b7348128af81b%3A0xc1bf2f669c38c5db!2sRESTAURANTE%20DO%C3%91A%20BARBARA%2C%20Guadalajara%2C%20Yopal%2C%20Casanare!3m2!1d5.3617878999999995!2d-72.3200942!5e0!3m2!1ses!2sco!4v1568142654501!5m2!1ses!2sco',
        }
    },
    template: `<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card mb-1 py-1 border-left-primary">
        <div class="card-body">
            <center>
                <h1 class="h4 mb-1 text-gray-800">Nuevo Concurso / Selección de Rutas</h1>
            </center>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-lg-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ruta 1 / Visualización</h6>
                </div>
                <div class="card-body">
                    <center>
                        <iframe :src="mapa" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ruta 2 / Visualización</h6>
                </div>
                <div class="card-body">
                    <center>
                        <iframe :src="mapa" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ruta 3 / Visualización</h6>
                </div>
                <div class="card-body">
                    <center>
                        <iframe :src="mapa" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <!-- Page Heading -->
    <div class="card mb-1 py-1 border-left-primary">
        <div class="card-body">
            <center>
                <h1 class="h4 mb-1 text-gray-800">Nuevo Concurso / Datos Generales</h1>
            </center>
        </div>
    </div>
    <hr>

    

    <!-- Content Row -->
    <div class="row">

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ruta Seleccionada / Vizualización</h6>
                </div>
                <div class="card-body">
                    <center>
                        <iframe :src="mapa" width="100%" height="420" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </center>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Datos Generales</h6>
                </div>
                <div class="card-body">
                    <form class="user">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nombre..." id="nombre_concurso">
                        </div>
                        <hr>
                        <div class="form-group">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" id="imagen_concurso" @change="base64imagen($event.target.files[0])">
                        </div>
                        <hr>
                        <div class="form-group">        
                                <input type="number" class="form-control" placeholder="Valor Inscripcion" id="ValorInscripcion_concurso">
                            </div>
                            <hr>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Descripción y Patrocinadores..." id="descripcion_concurso"></textarea>
                        </div>
                        <hr> 
                        <div class="form-group">
                            <label>Fecha Finalizacion De Inscripcion:</label>
                            <input type="date" class="form-control" id="fecha_finalizacion_concurso">
                        </div>
                        <hr>                       
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Premio..." id="premio_concurso"></textarea>
                        </div>
                        <hr>                       
                        <button class="btn btn-primary btn-user btn-block" @click="registrarConcurso()">Registrar</button>
                        <hr>
                        </a>
                        
                    </form>
                </div>
            </div>
        </div>
        
        

    </div>
    <hr>

    <!-- IMAGEN PROMOCIONAL - PREVIEW-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Imagen Promocional / Vizualización</h6>
                </div>
                <div class="card-body" id="img_view">
                    <center><img class="nuevo_concurso_img" src="img/nuevo_concurso/sin.jpg" ></center>
                </div>
            </div>
        </div>
    </div>



  </div>`,
 
  data(){
    return{
        Concurso: [],
        nombre_concurso: null,        
        imagen_concurso: null,
        ValorInscripcion_concurso: null,
        descripcion_concurso: null,
        fecha_finalizacion_concurso: null,
        premio_concurso: null,
        
    }
  },
  created() {
      let datosDB = JSON.parse(localStorage.getItem('bikerx-Concurso'));
      if(datosDB === null){
          this.Concurso = [];
      }else{
          this.Concurso = datosDB;
      }
  },
  
  methods: {
    base64imagen: function (file) {
        var canvas = document.getElementById("canvas");
        var ctx = canvas.getContext("2d");
        var cw = canvas.width;
        var ch = canvas.height;
        var maxW = 1280;
        var maxH = 720;
        var img_final = '';

        var img = new Image;
        img.onload = function () {
            var iw = img.width;
            var ih = img.height;
            var scale = Math.min((maxW / iw), (maxH / ih));
            var iwScaled = iw * scale;
            var ihScaled = ih * scale;
            canvas.width = iwScaled;
            canvas.height = ihScaled;
            ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
            // ASIGNACIÓN DE IMG A BASE 64 A LAS VARIABLES
            img_final = canvas.toDataURL("image/jpeg", 0.5);
            cargarImg(img_final);
            return;

            
        }
        img.src = URL.createObjectURL(file);
    },
    registrarConcurso(){
        this.Concurso.push({
            nombre_concurso: nombre_concurso.value,
            imagen_concurso: img_nuevo_concurso,
            ValorInscripcion_concurso: ValorInscripcion_concurso.value,
            descripcion_concurso: descripcion_concurso.value,
           fecha_finalizacion_concurso: fecha_finalizacion_concurso.value,
           premio_concurso: premio_concurso.value,
        });

        nombre_concurso.value = '';
        img_nuevo_concurso = null;
        ValorInscripcion_concurso.value = '';
        descripcion_concurso.value = '';
       fecha_finalizacion_concurso.value = '';
       premio_concurso.value = '';

        

        localStorage.setItem('bikerx-Concurso', JSON.stringify(this.Concurso));

        Swal.fire({
            position: 'center',
            type: 'success',
            title: 'Nuevo Concurso registrado',
            showConfirmButton: false,
            timer: 1500
          })
        return;

    }
  
  },
  
  
});

function cargarImg(img_final){
    document.getElementById('img_view').innerHTML = '<center><img v-else class="nuevo_concurso_img" src="'+img_final+'"></center>';
    img_nuevo_concurso = img_final;
    return;
}