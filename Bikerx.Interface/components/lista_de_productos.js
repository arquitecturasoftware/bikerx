Vue.component('lista_de_productos', {
    template: `<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card mb-1 py-1 border-left-primary">
        <div class="card-body">
            <center>
                <h1 class="h4 mb-1 text-gray-800">Productos</h1>
            </center>
        </div>
    </div>
    <hr>

    <!-- Content Row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Tabla / Productos</h6>
                </div>  
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Redimible</th>
                                <th>Vencimiento</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Redimible</th>
                                <th>Vencimiento</th>
                                <th>Opciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr v-for="(producto, index) in productos">
                                <td>{{index}}</td>
                                <td>{{producto.nombre_producto}}</td>
                                <td>{{producto.precio_producto}}</td>
                                <td>{{producto.producto_redimible}}</td>
                                <td>{{producto.fecha_vencimiento_producto}}</td>
                                <td>
                                    <button class="btn btn-circle btn-info" @click="consultarProducto(index)">i</button>
                                    <button class="btn btn-circle btn-warning" data-toggle="modal" data-target="#editarProducto" @click="preEditarProducto(index)">+</button>
                                    <button class="btn btn-circle btn-danger" @click="eliminarProducto(index)">x</button>
                                </td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <hr>

    <!-- MODAL EDITAR PRODUCTO -->
    <div class="modal fade" id="editarProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Lista de Prodcutos / Editar Producto / {{key_producto}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-lg-12">
                  <div class="card shadow mb-4">
                      <div class="card-header py-3">
                          <h6 class="m-0 font-weight-bold text-primary">Visualización de la Imagen</h6>
                      </div>
                      <div class="card-body" id="img_view">
                          <center><img class="nuevo_producto_img" src="img/nuevo_producto/sin_imagen.jpg"></center>
                      </div>
                  </div>
              </div>
  
              <div class="col-lg-12">
                  <div class="card shadow mb-4">
                      <div class="card-header py-3">
                          <h6 class="m-0 font-weight-bold text-primary">Datos Generales</h6>
                      </div>
                      <div class="card-body">
  
                              <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Nombre" id="nombre_producto">
                              </div>
                              <hr>
                              <div class="form-group">
                                  <label>Imagen:</label>
                                  <input type="file" class="form-control" id="imagen_producto" @change="base64imagen($event.target.files[0])">
                              </div>
                              <hr>
                              <div class="form-group">
                                  <label>¿Es redimible?</label>
                                  <select class="form-control" id="producto_redimible" @change="esRedimible()">
                                      <option>No</option>
                                      <option>Si</option>
                                  </select>
                              </div>
                              <hr>
                              <div class="form-group row">
                                  <div class="col">
                                      <input type="number" class="form-control" placeholder="Precio" id="precio_producto">
                                  </div>
                                  
                                  <div class="col" v-show="producto_redimible == 'Si'">
                                      <input type="number" class="form-control" placeholder="Costo en puntos" id="costo_puntos_producto">
                                  </div>
                              </div>
                              <hr>
                              <div class="form-group">
                                  <label>Fecha de vencimiento:</label>
                                  <input type="date" class="form-control" id="fecha_vencimiento_producto">
                              </div>
                      </div>
                  </div>
              </div>
  
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" @click="editarProducto(key_producto)">Guardar Cambios</button>
        </div>
      </div>
    </div>
  </div>

  </div>`,
  data(){
    return{
        productos: [],
        key_producto : null,
        producto_redimible: 'Si',
    }
  },
  created() {
      let datosDB = JSON.parse(localStorage.getItem('bikerx-productos'));
      if(datosDB === null){
          this.productos = [];
      }else{
          this.productos = datosDB;
      }
  },
  methods: {
      consultarProducto(id){
        Swal.fire({
            title: this.productos[id].nombre_producto,
            html: '<p>Precio: <strong>$'+this.productos[id].precio_producto+'</strong></p><p>Redimible: <strong>'+this.productos[id].producto_redimible+'</strong></p><p>Puntos: <strong>'+this.productos[id].costo_puntos_producto+'</strong></p> <p>Fecha de Vencimiento: <strong>'+this.productos[id].fecha_vencimiento_producto+'</strong></p>',
            imageUrl: this.productos[id].imagen_producto,
            imageWidth: '100%',
            imageAlt: this.productos[id].nombre_producto,
            animation: true
          })
      },
      preEditarProducto(id){
        let timerInterval
        Swal.fire({
            timer: 1500,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.timer
            ) {
                console.log('I was closed by the timer')
            }
        })
        this.key_producto = id;
        nombre_producto.value = this.productos[id].nombre_producto;
        img_nuevo_producto = this.productos[id].imagen_producto;
        document.getElementById('img_view').innerHTML = '<center><img v-else class="nuevo_producto_img" src="'+img_nuevo_producto+'"></center>';
        producto_redimible.value = this.productos[id].producto_redimible;
        this.producto_redimible = this.productos[id].producto_redimible;
        precio_producto.value = this.productos[id].precio_producto;
        costo_puntos_producto.value = this.productos[id].costo_puntos_producto;
        fecha_vencimiento_producto.value = this.productos[id].fecha_vencimiento_producto;
      },
        base64imagen: function (file) {
            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            var cw = canvas.width;
            var ch = canvas.height;
            var maxW = 1280;
            var maxH = 720;
            var img_final = '';

            var img = new Image;
            img.onload = function () {
                var iw = img.width;
                var ih = img.height;
                var scale = Math.min((maxW / iw), (maxH / ih));
                var iwScaled = iw * scale;
                var ihScaled = ih * scale;
                canvas.width = iwScaled;
                canvas.height = ihScaled;
                ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
                // ASIGNACIÓN DE IMG A BASE 64 A LAS VARIABLES
                img_final = canvas.toDataURL("image/jpeg", 0.5);
                cargarImg(img_final);
                return;
            }
            img.src = URL.createObjectURL(file);
        },
      editarProducto(id){
        this.productos[id].nombre_producto = nombre_producto.value;
        this.productos[id].imagen_producto = img_nuevo_producto;
        this.productos[id].producto_redimible = producto_redimible.value;
        this.productos[id].precio_producto = precio_producto.value;
        this.productos[id].costo_puntos_producto = costo_puntos_producto.value;
        this.productos[id].fecha_vencimiento_producto = fecha_vencimiento_producto.value;
        localStorage.setItem('bikerx-productos', JSON.stringify(this.productos));
        $('#editarProducto').modal('hide');
        Swal.fire({
            position: 'center',
            type: 'success',
            title: 'Cambios guardados',
            showConfirmButton: false,
            timer: 1500
          })
        return;
      },
      eliminarProducto(id){
        Swal.fire({
            title: '¿Estas seguro?',
            text: "¡No podras revertir esto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar',
            cancelButtonText: 'No, cancelar'
          }).then((result) => {
            if (result.value) {
                this.productos.splice(id, 1);
                localStorage.setItem('bikerx-productos', JSON.stringify(this.productos));
                Swal.fire(
                    '¡Eliminado!',
                    'El registro ha sido eliminado',
                    'success'
                )
            }
          })


          
      }
  },
});