Vue.component('barra_navegacion_lateral', {
    template: `<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#" @click="redireccionar('dashboard')">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-bicycle"></i>
      </div>
      <div class="sidebar-brand-text mx-3"><img class="logo_navegacion_lateral" src="img/logo.png"></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Módulos
    </div>

    <li class="nav-item active">
      <a class="nav-link" href="#" @click="redireccionar('inicio')">
        <i class="fas fa-fw fa-home"></i>
        <span>Inicio</span></a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#mis_productos" aria-expanded="true" aria-controls="mis_productos">
        <i class="fas fa-fw fa-table"></i>
        <span>Mis Productos</span>
      </a>
      <div id="mis_productos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Componentes:</h6>
          <a class="collapse-item" href="#"  @click="redireccionar('nuevo_producto')">Nuevo Producto</a>
          <a class="collapse-item" href="#"  @click="redireccionar('lista_de_productos')">Lista de Productos</a>
        </div>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="#" @click="redireccionar('mi_ubicacion')">
        <i class="fas fa-fw fa-map-marker"></i>
        <span>Mi Ubicación</span></a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#mis_concursos" aria-expanded="true" aria-controls="mis_concursos">
        <i class="fas fa-fw fa-trophy"></i>
        <span>Mis Concursos</span>
      </a>
      <div id="mis_concursos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Componentes:</h6>
          <a class="collapse-item" href="#"  @click="redireccionar('nuevo_concurso')">Nuevo Concurso</a>
          <a class="collapse-item" href="#"  @click="redireccionar('lista_de_concursos')">Lista de Concursos</a>
        </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Heading -->
    <div class="sidebar-heading">
      Dashboard
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
      <a class="nav-link" href="#" @click="redireccionar('dashboard')">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>
        <span>Components</span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Components:</h6>
          <a class="collapse-item" href="#"  @click="redireccionar('botones')">Buttons</a>
          <a class="collapse-item" href="#"  @click="redireccionar('cartas')">Cards</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Utilities</span>
      </a>
      <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Utilities:</h6>
          <a class="collapse-item" href="#" @click="redireccionar('utilidades_colores')">Colors</a>
          <a class="collapse-item" href="#" @click="redireccionar('utilidades_bordes')">Borders</a>
          <a class="collapse-item" href="#" @click="redireccionar('utilidades_animaciones')">Animations</a>
          <a class="collapse-item" href="#" @click="redireccionar('utilidades_otras')">Other</a>
        </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Addons
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Pages</span>
      </a>
      <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Login Screens:</h6>
          <a class="collapse-item" href="login.html">Login</a>
          <a class="collapse-item" href="register.html">Register</a>
          <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
          <div class="collapse-divider"></div>
          <h6 class="collapse-header">Other Pages:</h6>
          <a class="collapse-item" href="404.html">404 Page</a>
          <a class="collapse-item" href="#" @click="redireccionar('pagina_en_blanco')">Blank Page</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
      <a class="nav-link" href="#" @click="redireccionar('graficas')">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Charts</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
      <a class="nav-link" href="#" @click="redireccionar('tablas')">
        <i class="fas fa-fw fa-table"></i>
        <span>Tables</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>`,
  methods: {
    redireccionar(direccion){
      bikerx.seleccionado = direccion;
    }
  },
});