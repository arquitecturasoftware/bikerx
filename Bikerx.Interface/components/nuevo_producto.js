var img_nuevo_producto = null;

Vue.component('nuevo_producto', {
    template: `<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card mb-1 py-1 border-left-primary">
        <div class="card-body">
            <center>
                <h1 class="h4 mb-1 text-gray-800">Nuevo Producto</h1>
            </center>
        </div>
    </div>
    <hr>

    <!-- Content Row -->
    <div class="row">

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Visualización de la Imagen</h6>
                </div>
                <div class="card-body" id="img_view">
                    <center><img class="nuevo_producto_img" src="img/nuevo_producto/sin_imagen.jpg"></center>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Datos Generales</h6>
                </div>
                <div class="card-body">

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nombre" id="nombre_producto">
                        </div>
                        <hr>
                        <div class="form-group">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" id="imagen_producto" @change="base64imagen($event.target.files[0])">
                        </div>
                        <hr>
                        <div class="form-group">
                            <label>¿Es redimible?</label>
                            <select class="form-control" id="producto_redimible" @change="esRedimible()">
                                <option>No</option>
                                <option>Si</option>
                            </select>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col">
                                <input type="number" class="form-control" placeholder="Precio" id="precio_producto">
                            </div>
                            
                            <div class="col" v-show="producto_redimible == 'Si'">
                                <input type="number" class="form-control" placeholder="Costo en puntos" id="costo_puntos_producto">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label>Fecha de vencimiento:</label>
                            <input type="date" class="form-control" id="fecha_vencimiento_producto">
                        </div>
                        <hr>
                        
                        <button class="btn btn-primary btn-user btn-block" @click="registrarProducto()">Registrar</button>
                        <hr>
                 
                </div>
            </div>
        </div>

    </div>
    <hr>

  </div>`,
  data(){
    return{
        productos: [],
        nombre_producto: null,
        imagen_producto: null,
        producto_redimible: 'No',
        precio_producto: null,
        costo_puntos_producto: null,
        fecha_vencimiento_producto: null,
    }
  },
  created() {
      let datosDB = JSON.parse(localStorage.getItem('bikerx-productos'));
      if(datosDB === null){
          this.productos = [];
      }else{
          this.productos = datosDB;
      }
  },
  methods: {
    base64imagen: function (file) {
        var canvas = document.getElementById("canvas");
        var ctx = canvas.getContext("2d");
        var cw = canvas.width;
        var ch = canvas.height;
        var maxW = 1280;
        var maxH = 720;
        var img_final = '';

        var img = new Image;
        img.onload = function () {
            var iw = img.width;
            var ih = img.height;
            var scale = Math.min((maxW / iw), (maxH / ih));
            var iwScaled = iw * scale;
            var ihScaled = ih * scale;
            canvas.width = iwScaled;
            canvas.height = ihScaled;
            ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
            // ASIGNACIÓN DE IMG A BASE 64 A LAS VARIABLES
            img_final = canvas.toDataURL("image/jpeg", 0.5);
            cargarImg(img_final);
            return;
        }
        img.src = URL.createObjectURL(file);
    },
    esRedimible(){
        this.producto_redimible = producto_redimible.value;
    },
    registrarProducto(){
        this.productos.push({
            nombre_producto: nombre_producto.value,
            imagen_producto: img_nuevo_producto,
            producto_redimible: producto_redimible.value,
            precio_producto: precio_producto.value,
            costo_puntos_producto: costo_puntos_producto.value,
            fecha_vencimiento_producto: fecha_vencimiento_producto.value
        });

        nombre_producto.value = '';
        imagen_producto.value = '';
        img_nuevo_producto = null;
        producto_redimible.value = 'No';
        this.producto_redimible = producto_redimible.value;
        precio_producto.value = '';
        costo_puntos_producto.value = '';
        fecha_vencimiento_producto.value = '';

        localStorage.setItem('bikerx-productos', JSON.stringify(this.productos));

        Swal.fire({
            position: 'center',
            type: 'success',
            title: 'Nuevo producto registrado',
            showConfirmButton: false,
            timer: 1500
          })
        return;

    }
  },
  
});


function cargarImg(img_final){
    document.getElementById('img_view').innerHTML = '<center><img v-else class="nuevo_producto_img" src="'+img_final+'"></center>';
    img_nuevo_producto = img_final;
    return;
}