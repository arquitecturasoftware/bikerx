Vue.component('lista_de_concursos', {
    template: `<div class="container-fluid">

  
  <!-- Page Heading -->
  <h1 class="h3 mb-4 text-gray-800">Lista De Los Concursos</h1>


  <!-- DataTales Example -->
  <div class="card shadow mb-4">
          <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nombre del concurso</th>
              <th>Valor Concurso</th>
              <th>Finalizacion Concurso</th>               
          </tr>
          </thead>
          <tbody>
          <tr v-for="(concurso, index) in concurso">
              <td>{{index}}</td>
              <td>{{Concurso.nombre_concurso}}</td>
              <td>{{Concurso.ValorInscripcion_concurso}}</td>
              <td>{{Concurso.fecha_finalizacion_concurso}}</td>
              <td>
                  <button class="btn btn-circle btn-info" @click="consultarconcurso(index)">i</button>
                  <button class="btn btn-circle btn-warning" data-toggle="modal" data-target="#editarconcurso" @click="preEditarconcurso(index)">+</button>
                  <button class="btn btn-circle btn-danger" @click="eliminarconcurso(index)">x</button>
              </td>
          </tr>
      </tbody>   
          </table>
        </div>
      </div>
    </div>
</div>

<!-- EDITAR CONCURSO -->
<div class="modal fade" id="editarconcurso" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLongTitle">Lista de Concurso / Editar Concurso / {{key_concurso}}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="row">
          <div class="col-lg-12">
              <div class="card shadow mb-4">
                  <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Visualización de la Imagen</h6>
                  </div>
                  <div class="card-body" id="img_view">
                      <center><img class="nuevo_concurso_img" src="img/nuevo_concurso/sin.jpg"></center>
                  </div>
              </div>
          </div>

          <div class="col-lg-12">
              <div class="card shadow mb-4">
                  <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Datos Generales</h6>
                  </div>
                  <div class="card-body">

                          <div class="form-group">
                              <input type="text" class="form-control" placeholder="Nombre" id="nombre_concurso">
                          </div>
                          <hr>
                          <div class="form-group">
                              <label>Imagen:</label>
                              <input type="file" class="form-control" id="imagen_concurso" @change="base64imagen($event.target.files[0])">
                          </div>
                                                  
                          <hr>
                          <div class="form-group row">
                              <div class="col">
                                  <input type="number" class="form-control" placeholder="Valor Inscripcion" id="Valor_Inscripcion_concurso">
                              </div>
                                                           
                          </div>
                          <hr>
                          <div class="form-group">
                              <label>Fecha finalizacion concurso:</label>
                              <input type="date" class="form-control" id="fecha_finalizacion_concurso">
                          </div>
                  </div>
              </div>
          </div>

      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      <button type="button" class="btn btn-primary" @click="editarconcurso(key_concurso)">Guardar Cambios</button>
    </div>
  </div>
</div>
</div>

</div>`,
data(){
  return{
      Concurso: [],
      key_concurso : null,
  }
},
created() {
    let datosDB = JSON.parse(localStorage.getItem('bikerx-concurso'));
    if(datosDB === null){
        this.Concurso = [];
    }else{
        this.Concurso = datosDB;
    }
},
methods: {
    consultarconcurso(id){
      Swal.fire({
          title: this.Concurso[id].nombre_concurso,
          html: '<p>ValorInscripcion: <strong>$'+this.Concurso[id].ValorInscripcion_concurso+ '<p>Fecha de finalizacion: <strong>'+this.Concurso[id].fecha_finalizacion_concurso+'</strong></p>',
          imageUrl: this.Concurso[id].imagen_concurso,
          imageWidth: '100%',
          imageAlt: this.Concurso[id].nombre_concurso,
          animation: true
        })
    },
    preEditarconcurso(id){
      let timerInterval
      Swal.fire({
          timer: 1500,
          onBeforeOpen: () => {
              Swal.showLoading()
          },
          onClose: () => {
              clearInterval(timerInterval)
          }
          }).then((result) => {
          if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.timer
          ) {
              console.log('I was closed by the timer')
          }
      })
      this.key_concurso = id;
      nombre_concurso.value = this.Concurso[id].nombre_concurso;
      img_nuevo_concurso = this.Concurso[id].imagen_concurso;
      document.getElementById('img_view').innerHTML = '<center><img v-else class="nuevo_concurso_img" src="'+img_nuevo_concurso+'"></center>'
      ValorInscripcion_concurso.value = this.Concurso[id].ValorInscripcion_concurso;
      descripcion_concurso.value = this.Concurso[id].descripcion_concurso;
      fecha_finalizacion_concurso.value = this.Concurso[id].fecha_finalizacion_concurso;
      premio_concurso.value = this.Concurso[id].premio_concurso;
    },
      base64imagen: function (file) {
          var canvas = document.getElementById("canvas");
          var ctx = canvas.getContext("2d");
          var cw = canvas.width;
          var ch = canvas.height;
          var maxW = 1280;
          var maxH = 720;
          var img_final = '';

          var img = new Image;
          img.onload = function () {
              var iw = img.width;
              var ih = img.height;
              var scale = Math.min((maxW / iw), (maxH / ih));
              var iwScaled = iw * scale;
              var ihScaled = ih * scale;
              canvas.width = iwScaled;
              canvas.height = ihScaled;
              ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
              // ASIGNACIÓN DE IMG A BASE 64 A LAS VARIABLES
              img_final = canvas.toDataURL("image/jpeg", 0.5);
              cargarImg(img_final);
              return;
          }
          img.src = URL.createObjectURL(file);
      },
    editarconcurso(id){
      this.Concurso[id].nombre_concurso = nombre_concurso.value;
      this.Concurso[id].imagen_concurso = img_nuevo_concurso;
      this.Concurso[id].ValorInscripcion_concurso = ValorInscripcion_concurso.value;
      this.Concurso[id].descripcion_concurso = descripcion_concurso.value;
      this.Concurso[id].fecha_finalizacion_concurso = fecha_finalizacion_concurso.value;
      this.Concurso[id].premio_concurso = premio_concurso.value;
      localStorage.setItem('bikerx-Concurso', JSON.stringify(this.Concurso));
      $('#editarConcurso').modal('hide');
      Swal.fire({
          position: 'center',
          type: 'success',
          title: 'Cambios guardados',
          showConfirmButton: false,
          timer: 1500
        })
      return;
    },
    eliminarconcurso(id){
      Swal.fire({
          title: '¿Estas seguro?',
          text: "¡No podras revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí, eliminar',
          cancelButtonText: 'No, cancelar'
        }).then((result) => {
          if (result.value) {
              this.productos.splice(id, 1);
              localStorage.setItem('bikerx-Concurso', JSON.stringify(this.Concurso));
              Swal.fire(
                  '¡Eliminado!',
                  'El registro ha sido eliminado',
                  'success'
              )
          }
        })


        
    }
},

});